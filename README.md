Author: [Robinson Dias](https://github.com/robinson-1985)

# Criando uma aplicação de transferências bancárias com .NET - Digital Innovation One

## POO na prática: criando uma aplicação

- Aprendemos nesse projeto como criar um algoritmo simples de transferência bancária para 
exercer o pensamento orientado a objetos, o principal paradigma de programação utilizada 
no mercado. Nesse projeto aprendemos: Como pensar orientado a objetos, como modelar o seu 
domínio, como utilizar enums.

## Bootcamp Decola Tech 2021

- Curso no link: [https://web.digitalinnovation.one/lab/criando-uma-aplicacao-que-simula-transferencias-entre-contas-bancarias-com-net/learning/0229f417-f760-4f5a-93b6-442e0c658c8f]

- Instrutor expert DIO: Eliézer Zarpelão

Contato com o instrutor pelo Linkedin:  [br.linkedin.com/in/eliezerzarpelao](http://br.linkedin.com/in/eliezerzarpelao)


